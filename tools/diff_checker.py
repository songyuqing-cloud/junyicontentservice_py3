import os
import sys
import argparse


def get_local_diff_python_files():
    stdout = os.popen('git diff --name-only | grep .py$').read()
    return [i for i in stdout.split('\n') if i]


def get_origin_diff_python_files(target_branch):
    # Get the changes between origin/staging and HEAD, starting at a common ancestor of both
    # https://git-scm.com/docs/git-diff#Documentation/git-diff.txt-emgitdiffemltoptionsgtltcommitgtltcommitgt--ltpathgt82308203
    stdout = os.popen('git diff --name-only origin/%s...HEAD | grep .py$' % target_branch).read()
    return [i for i in stdout.split('\n') if i]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target_branch',
                        required=False,
                        type=str,
                        default='staging',
                        dest="target_branch")
    args = parser.parse_args()
    local_diff_files = get_local_diff_python_files()
    origin_diff_files = get_origin_diff_python_files(args.target_branch)
    diff_files = set(local_diff_files+origin_diff_files)
    sys.stdout.write(' '.join(diff_files))


if __name__ == '__main__':
    main()
