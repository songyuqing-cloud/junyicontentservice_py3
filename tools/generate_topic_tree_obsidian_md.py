import json
import sys
import time
from pathlib import Path

DIR_ROOT = 'obs'
DIR_KINDS = (
    'Exercise',
    'Video',
    'Exam',
    'Divider',
    'Url',
    'Article',
)


def read_json(filename):
    with open(filename, 'r') as f:
        return json.load(f)


def make_dirs():
    for kind in DIR_KINDS:
        Path(f'{DIR_ROOT}/{kind}').mkdir(parents=True, exist_ok=True)


def write_topic(data, n=0):
    out = format_props(data, ('id', 'title')) + \
          format_children(data) + \
          format_topic_hashtags(data, n)
    filename = get_filename(data)
    write_md(filename, out)
    write_children(data, n)


def write_content(data):
    out = format_props(data, ('kind', 'id', 'title', 'is_live')) + \
          format_content_hashtags(data)
    filename = get_filename(data)
    write_md(filename, out)


def write_children(data, n):
    if data['is_content_topic']:
        for child in data['children']:
            write_content(child)
    else:
        for child in data['children']:
            write_topic(child, n + 1)


def format_props(data, props):
    out = ''
    for k in props:
        out += f'- {k}: {data[k]}\n'
    return out


def format_topic_hashtags(data, n):
    out = '\n'
    if data['is_content_topic']:
        out += '#content_topic\n'
    out += f'#level_{n}\n'
    return out


def format_content_hashtags(data):
    out = '\n'
    if not data['is_live']:
        out += '#hidden\n'
    return out


def format_children(data):
    if data['is_content_topic']:
        return format_child_contents(data['children'])
    else:
        return format_child_topics(data['children'])


def format_child_topics(child_topics):
    if not child_topics:
        return ''
    out = '- child_topics:\n'
    for c in child_topics:
        id = c['id']
        out += f'  - [[{id}]]\n'
    return out


def format_child_contents(child_contents):
    if not child_contents:
        return ''
    out = '- child_contents:\n'
    for c in child_contents:
        id = c['id']
        out += f'  - [[{id}]]\n'
    return out


def get_filename(data):
    filename = 'obs'
    if 'kind' in data:
        kind = data['kind']
        filename += f'/{kind}'
    id = data['id']
    filename += f'/{id}.md'
    return filename


def write_md(filename, data):
    print(f'Creating {filename}...')
    with open(filename, 'w') as out:
        out.write(data)
    time.sleep(0)


filename = sys.argv[1]
data = read_json(filename)

make_dirs()
write_topic(data)
