# -*- coding: utf-8 -*-
from typing import Optional


class Content:
    _property_from_entity = []
    _entity = None
    _parent_topic = None

    @property
    def is_live(self):
        raise NotImplementedError

    @property
    def is_teaching_material(self):
        raise NotImplementedError

    def __init__(self, entity, parent_topic=None):
        self._entity = entity
        for prop in self._property_from_entity:
            setattr(self, prop, self._entity.get(prop))
        self._parent_topic = parent_topic

    def info_to_topic_page(self):
        raise NotImplementedError

    def dump(self, props: Optional[list] = None) -> dict:
        """Dump properties to dict; subclasses are allowed to inherit and customize it"""
        props_to_dump = props or self._property_from_entity
        ret = {prop: getattr(self, prop, None) for prop in props_to_dump}
        ret['kind'] = self.__class__.__name__  # The result will be the subclass's name, like 'Video' or 'Exercise'
        return ret
