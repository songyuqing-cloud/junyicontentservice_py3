# -*- coding: utf-8 -*-
import unittest
from unittest import mock
import urllib.error

from . import youtube_sync

class TestYoutubeSync(unittest.TestCase):
    def test_ok(self):
        info = youtube_sync.get_video_info_by_youtube_api('NehkLV77ITk')
        self.assertEqual(info['id'], 'NehkLV77ITk')

    @mock.patch('urllib.request.urlopen')
    def test_fail(self, urlopen_patch):
        urlopen_patch.side_effect = urllib.error.HTTPError(
            url='url', code=500, msg='msg', hdrs=None, fp=None)

        info = youtube_sync.get_video_info_by_youtube_api('NehkLV77ITk')
        self.assertIsNone(info)
