# -*- coding: utf-8 -*-

from flask import current_app
from google.cloud import datastore

from . import internal

SOCRATES_QUESTION_ROOT = 'socrates_root'


class SocratesQuestion(object):
    """
    Socrates

    Database entity about a single socrates question (quiz in video)
    """
    # TODO: add model properties

    @staticmethod
    def exists_for_video(readable_id: str) -> bool:
        ancestor_key = SocratesQuestion._get_parent_key()
        query = internal.get_query(kind='SocratesQuestion', ancestor=ancestor_key)
        query.add_filter('readable_id', '=', readable_id)
        query.add_filter('live', '=', True)
        return len(list(query.fetch(1))) > 0

    @staticmethod
    def _get_parent_key() -> datastore.Key:
        # a fake parent entity who is parent of all socrates question entities
        project = 'junyiacademytest1' if current_app.config.get('IN_TEST') else 'junyiacademy'
        return datastore.Key('parents', SOCRATES_QUESTION_ROOT, project=project)
