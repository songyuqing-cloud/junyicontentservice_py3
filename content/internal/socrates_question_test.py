# -*- coding: utf-8 -*-
import unittest
from unittest import mock

from . import socrates_question


class TestSocratesQuestion(unittest.TestCase):
    @mock.patch('content.internal.internal.get_query')
    def test_exists_for_video_true(self, get_query_patch):
        # mock fetch
        query_fetch_patch = mock.Mock()
        query_fetch_patch.return_value = [1]
        # mock query
        query_patch = mock.Mock()
        query_patch.fetch = query_fetch_patch
        # mock get_query
        get_query_patch.return_value = query_patch

        # action
        fake_readable_id = 'a'
        exists = socrates_question.SocratesQuestion.exists_for_video(fake_readable_id)

        # assert
        get_query_patch.assert_called()
        self.assertEqual(get_query_patch.call_args[1]['kind'], 'SocratesQuestion')
        query_patch.add_filter.assert_called()
        self.assertEqual(query_patch.add_filter.call_args_list[0][0], ('readable_id', '=', fake_readable_id))
        self.assertEqual(query_patch.add_filter.call_args_list[1][0], ('live', '=', True))
        query_fetch_patch.assert_called_once_with(1)
        self.assertTrue(exists)

    @mock.patch('google.cloud.datastore.query.Query.fetch')
    def test_exists_for_video_false(self, query_fetch_patch):
        query_fetch_patch.return_value = []
        exists = socrates_question.SocratesQuestion.exists_for_video('a')
        query_fetch_patch.assert_called_once_with(1)
        self.assertFalse(exists)

    @mock.patch('flask.current_app.config.get')
    def test_get_parent_key(self, config_patch):
        # in test server
        config_patch.return_value = True
        key = socrates_question.SocratesQuestion._get_parent_key()
        config_patch.assert_called_once_with('IN_TEST')
        self.assertEqual(key.project, 'junyiacademytest1')
        # not in test server
        config_patch.return_value = False
        key = socrates_question.SocratesQuestion._get_parent_key()
        self.assertEqual(key.project, 'junyiacademy')
