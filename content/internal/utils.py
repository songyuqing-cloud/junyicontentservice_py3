from flask import current_app


def get_legacy_key_str(datastore_entity):
    if current_app.config.get('IN_LOCAL'):
        location_prefix = 'dev~'
    else:
        location_prefix = 's~'
    return datastore_entity.key.to_legacy_urlsafe(location_prefix=location_prefix).decode('utf-8')
