# -*- coding: utf-8 -*-
"""
Implements of every type of content
"""
from .sheet_change import SetRow, SheetMeta

class SetContentDomain(SetRow):
    """
    Parse content domain
    """
    topic_type = 'ContentDomain'

    def set_topic(self, topic_dict, subject=''):
        self.set_row(topic_dict)
        self.clear()
        self.sheet_table.add_topic(
            self.copy_topic_row(topic_dict, self.topic_type)
        )
        self.sheet_table.content_row['content_index'] = 0

class SetMainTopic(SetRow):
    """
    Parse main topic
    """

    topic_type = 'MainTopic'

    def set_topic(self, topic_dict, subject=''):
        self.set_row(topic_dict)
        self.sheet_table.topic_row['parent_id'] = self.sheet_table.topic_row['content_domain_id']
        self.clear(2)
        self.sheet_table.add_topic(
            self.copy_topic_row(topic_dict, self.topic_type)
        )
        self.sheet_table.content_row['content_index'] = 0

class SetChapter(SetRow):
    """
    Parse chapter
    """

    topic_type = 'Chapter'

    def set_topic(self, topic_dict, subject=''):
        self.set_row(topic_dict)
        self.sheet_table.topic_row['parent_id'] = self.sheet_table.topic_row['main_topic_id']
        self.sheet_table.content_row['grade'] \
            = self.sheet_table.topic_row['grade'] \
            = int(topic_dict['年級']) if ('年級' in topic_dict and topic_dict['年級']) else None

        self.clear(4, -1)
        self.sheet_table.add_topic(
            self.copy_topic_row(topic_dict, self.topic_type)
        )

        self.sheet_table.content_row['content_index'] = 0
        # 沒有 section 的 chapter 會沿用 chapter 的資訊當作 content row 的 section 初始資訊
        self.sheet_table.content_row['section_id'] = self.sheet_table.content_row['chapter_id']
        self.sheet_table.content_row['section_title'] \
            = self.sheet_table.content_row['chapter_title']

        if subject in SheetMeta.SUBJECTS_NEED_GRADE_INFO() \
            and self.sheet_table.content_row['grade'] is None:
            self.edu_error.add_error('次主題 %s[%s] 沒有填年級' % (
                self.sheet_table.content_row['chapter_title'],
                self.sheet_table.content_row['chapter_id'],
            ))

class SetSection(SetRow):
    """
    Parse section
    """
    topic_type = 'Section'

    def set_topic(self, topic_dict, subject=''):
        self.sheet_table.topic_row['section_id'] \
            = self.sheet_table.content_row['section_id'] \
            = topic_dict['代號']

        self.sheet_table.topic_row['section_id_by_grade'] \
            = self.sheet_table.content_row['section_id_by_grade'] \
            = topic_dict.get('年級式代號', None)

        self.sheet_table.topic_row['section_title'] \
            = self.sheet_table.content_row['section_title'] \
            = topic_dict.get('影片、知識點、講義', topic_dict['標題'])

        self.sheet_table.add_content(
            self.copy_content_row(topic_dict, self.topic_type)
        )

class SetContent(SetRow):
    """
    Parse content, which is same as default function
    """
    def set_topic(self, topic_dict, subject=''):
        pass
