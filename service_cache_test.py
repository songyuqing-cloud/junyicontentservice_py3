from unittest.mock import patch

import service_cache
from mock_cache import MockCache
from redis_cache import RedisCache
from testutil import testsize

try:
    import unittest2 as unittest
except ImportError:
    import unittest

# big_string is bigger than the 1MB memory cache and datastore limits and
# so chunks should get used
_BIG_STRING = "a" * 1000 * 1000 + "b" * 1000 * 1000

# huge_string is bigger than the 32MB limit for api requests that
# cache.set_multi would hit
_HUGE_STRING = "a" * 34 * 1000 * 1000


class ServiceCacheTest(unittest.TestCase):
    DBG = False

    def dbg(self, param):
        if self.DBG:
            print(param)

    def setUp(self):
        self.key = "__service_cache_service_cache_test.func__"
        super().setUp()
        service_cache.CACHE_CLASS = RedisCache
        self.dbg('')
        self.dbg('>>    ---- setUp ----')

    def tearDown(self):
        super(ServiceCacheTest, self).tearDown()
        service_cache.flush()
        self.dbg('>>    ---- tearDown ----')

    def _truncate_value(self, a):
        max_length = 100
        str_a = str(a)
        if len(str_a) <= max_length:
            return str_a
        else:
            return "%s(%i): '%s...%s'" % (
                a.__class__.__name__,
                len(a),
                str_a[:int(max_length / 2)],
                str_a[-int(max_length / 2):])

    def assertEqualTruncateError(self, a, b):
        assert a == b, "%s != %s" % (self._truncate_value(a),
                                     self._truncate_value(b))


class ServiceCacheBasicTest(ServiceCacheTest):
    def setUp(self):
        @service_cache.cache(compress_chunks=False)
        def func(result):
            return result

        self.cache_func = func
        super().setUp()

    def test_cache_should_return_cached_result(self):
        # test string
        self.cache_func("a")
        self.assertEqual("a", self.cache_func("b"))
        # test primitive types
        service_cache.flush()
        self.cache_func(1)
        self.assertEqual(1, self.cache_func(2))
        service_cache.flush()
        self.cache_func(True)
        self.assertEqual(True, self.cache_func(False))

    def test_large_cache_should_chunk_and_return_cached_result(self):
        self.cache_func(_BIG_STRING)
        self.assertIsNotNone(service_cache.CACHE_CLASS.get(self.key + "__chunk0__"))
        self.assertEqualTruncateError(_BIG_STRING, self.cache_func("a"))

    def test_should_throw_out_result_on_missing_chunk_and_reexecute(self):
        self.cache_func(_BIG_STRING)
        # deleting the 2nd chunk ... next time we get it pickle.loads should
        # throw an error and the target func will be rexecuted
        service_cache.CACHE_CLASS.delete(self.key + "__chunk1__")
        self.assertEqualTruncateError("a", self.cache_func("a"))

    @testsize.medium()
    def test_should_throw_out_result_when_wrong_chunk_is_read(self):
        ''' Tests to make sure results are recalculated when a chunk is corrupt

        If ChunkedResult in a race condition read from a previous version of a
        key then depickling should fail.  This will test that it failed
        silently and that the results are then recalculated.
        '''

        self.cache_func(_BIG_STRING)
        # overwriting the 1st chunk ... next time we get it pickle.loads should
        # throw an error and the target func will be rexecuted
        service_cache.CACHE_CLASS.set(self.key + "__chunk1__", "bad generation string")
        self.assertEqualTruncateError("a", self.cache_func("a"))

    def test_huge_cache_set_should_fail_gracefully_and_reexecute(self):
        self.cache_func(_HUGE_STRING)
        self.assertEqualTruncateError("a", self.cache_func("a"))

    @testsize.medium()
    def test_use_chunks_parameters_forces_chunking_for_small_size(self):
        @service_cache.cache(use_chunks=True)
        def func(result):
            return result

        func("a")
        self.assertIsInstance(service_cache.CACHE_CLASS.get(self.key),
                              service_cache.ChunkedResult)

    def test_disable_service_cache(self):
        self.cache_func("a")

        # not disabled by default
        self.assertFalse(service_cache.is_disabled())
        self.assertEqual("a", self.cache_func("b"))

        # still not disabled
        service_cache.enable()
        self.assertFalse(service_cache.is_disabled())
        self.assertEqual("a", self.cache_func("b"))

        # should be disabled
        service_cache.disable()
        self.assertTrue(service_cache.is_disabled())
        self.assertEqual("b", self.cache_func("b"))

        # should be not disabled
        service_cache.enable()
        self.assertFalse(service_cache.is_disabled())
        self.assertEqual("a", self.cache_func("b"))

    def test_corrupted_chunk_should_reexecute_target(self):
        # Tests whether depickle fails silently if data is corrupt.
        self.cache_func("stuff")
        # overwriting key value ... next time we get it depickle should
        # throw an error and the target func will be rexecuted
        service_cache.CACHE_CLASS.set(self.key,
                                      service_cache.ChunkedResult(
                                          data=bytes("depickle fail", 'ascii'),
                                          compress=False)
                                      )
        self.assertEqual("a", self.cache_func("a"))

    def test_one_chunk_result_deletes_successfully(self):
        self.cache_func(_BIG_STRING)
        service_cache.ChunkedResult.delete(self.key, cache_class=service_cache.CACHE_CLASS)
        # make sure target func re-evaluates now that we deleted the key
        self.assertEqual("a", self.cache_func("a"))

    @patch('service_cache.get_app_version')
    def test_cache_persist_across_app_versions(self, get_app_version_patch):
        @service_cache.cache(persist_across_app_versions=True)
        def func_persist(result):
            return result

        # test cache in app version `1`
        get_app_version_patch.return_value = '1'
        self.assertEqual("a", func_persist("a"))  # no cache
        self.assertEqual("a", func_persist("b"))  # from cache
        # test cache in app version `2`
        get_app_version_patch.return_value = '2'
        self.assertEqual("a", func_persist("c"))  # from cache

    @patch('service_cache.get_app_version')
    def test_cache_not_persist_across_app_versions(self, get_app_version_patch):
        @service_cache.cache(persist_across_app_versions=False)
        def func_not_persist(result):
            return result

        # test cache in app version `1`
        get_app_version_patch.return_value = '1'
        self.assertEqual("a", func_not_persist("a"))  # no cache
        self.assertEqual("a", func_not_persist("b"))  # from cache
        # test cache in app version `2`
        get_app_version_patch.return_value = '2'
        self.assertEqual("c", func_not_persist("c"))  # no cache

    def test_cache_skip_cache(self):
        @service_cache.cache()
        def func_test(result):
            return result

        self.assertEqual("a", func_test("a"))  # no cache
        self.assertEqual("a", func_test("b"))  # from cache
        self.assertEqual("b", func_test("b", skip_cache=True))  # skip cache # pylint: disable=E1123
        self.assertEqual("a", func_test("b", skip_cache=False))  # from cache # pylint: disable=E1123

    def test_cache_bust_cache(self):
        @service_cache.cache()
        def func_test(result):
            return result

        self.assertEqual("a", func_test("a"))  # no cache
        self.assertEqual("a", func_test("b"))  # from cache
        self.assertEqual("c", func_test("c", bust_cache=True))  # bust cache # pylint: disable=E1123
        self.assertEqual("c", func_test("d", bust_cache=False))  # from cache # pylint: disable=E1123


def test_mock_func():
    pass


class ServiceCacheFxnTest(ServiceCacheTest):
    def setUp(self):
        @service_cache.cache_with_key_fxn(
            lambda result: ("test_cache_fxn_%s" % result[:8]),
            compress_chunks=False)
        def func(result):
            test_mock_func()
            return result

        self.cache_func = func
        super().setUp()

    @patch('service_cache_test.test_mock_func')
    def test_cache_should_return_cached_result(self, patch_func):
        self.cache_func("a")
        patch_func.assert_called()

        patch_func.reset_mock()
        self.assertEqual("a", self.cache_func("a"))
        patch_func.assert_not_called()

        patch_func.reset_mock()
        self.assertEqual("a", self.cache_func("a", bust_cache=True))  # pylint: disable=E1123
        patch_func.assert_called()

        patch_func.reset_mock()
        self.assertEqual("b", self.cache_func("b"))
        patch_func.assert_called()


class ServiceCacheCompressionTest(ServiceCacheTest):
    def setUp(self):
        @service_cache.cache()
        def func(result):
            return result

        self.cache_func = func
        super().setUp()

    def test_corrupted_uncompressible_chunk_should_reexecute_target(self):
        '''Tests whether decompression fails silently if data is corrupt.

        It does an assert to see the results are recalculated when a chunk gets
        somehow corrupted causing decompression to fail.
        '''

        self.cache_func("stuff")
        # overwriting key value ... next time we get it decompress should
        # throw an error and the target func will be rexecuted
        service_cache.CACHE_CLASS.set(self.key,
                                      service_cache.ChunkedResult(data=bytes("decompress fail", 'ascii')))
        self.assertEqual("a", self.cache_func("a"))

    def test_one_chunk_result_deletes_successfully(self):
        self.cache_func(_BIG_STRING)

        service_cache.ChunkedResult.delete(self.key, cache_class=service_cache.CACHE_CLASS)

        # make sure target func re-evaluates now that we deleted the key
        self.assertEqual("a", self.cache_func("a"))


class ServiceCacheMockedTest(ServiceCacheBasicTest):
    def setUp(self):
        super(ServiceCacheMockedTest, self).setUp()
        service_cache.CACHE_CLASS = MockCache


class ServiceCacheFxnMockedTest(ServiceCacheFxnTest):
    def setUp(self):
        super(ServiceCacheFxnMockedTest, self).setUp()
        service_cache.CACHE_CLASS = MockCache


class ServiceCacheCompressionMockedTest(ServiceCacheCompressionTest):
    def setUp(self):
        super(ServiceCacheCompressionMockedTest, self).setUp()
        service_cache.CACHE_CLASS = MockCache
