.PHONY: run-ds test build_app

# Run datastore emulator
run-ds:
	gcloud beta emulators datastore start --data-dir=/work_resource --host-port=0.0.0.0:8085
	# Then you shoud do
	# $(gcloud beta emulators datastore env-init --data-dir=/work_resource)

# Run tests.  If COVERAGE is set (e.g. COVERAGE=true), run them in coverage mode.  If
# MAX_TEST_SIZE is set, only tests of this size or smaller are run.
MAX_TEST_SIZE = medium
test: build_app
	if test -n "$(COVERAGE)"; then  \
	   coverage3 run tools/runtests.py --max-size="$(MAX_TEST_SIZE)" && \
	   coverage3 report --skip-covered; \
	else \
	   python3 tools/runtests.py --max-size="$(MAX_TEST_SIZE)"; \
	fi

# Run pylint on all python files.
# Convention, refactoring, warning inspection are disabled in pylint, but are enabled in pylint-strict.
PY_SKIPDIRS += docs tools
PY_DIRS ?= $(filter-out $(PY_SKIPDIRS),$(subst /__init__.py,,$(wildcard */__init__.py)))
pylint-all:
	@echo Examing Python Modules and Files: $(PY_DIRS) $(wildcard *.py)
	python3 -m pylint --rcfile=.pylintrc --disable=C,R,W $(PY_DIRS) $(wildcard *.py)
pylint-all-strict:
	@echo Examing Python Modules and Files: $(PY_DIRS) $(wildcard *.py)
	python3 -m pylint --rcfile=.pylintrc $(PY_DIRS) $(wildcard *.py)

# Run pylint only on diff python files between current branch and staging.
# Convention, refactoring, warning inspection are disabled in pylint, but are enabled in pylint-strict.
pylint pylint-strict: DIFF_FILES=$(shell python3 tools/diff_checker.py)
pylint:
	if test -z "$(DIFF_FILES)"; then  \
	   echo "There is no change on python file."; \
	else \
	   python3 -m pylint --rcfile=.pylintrc --disable=C,R,W $(DIFF_FILES); \
	fi
pylint-strict:
	if test -z "$(DIFF_FILES)"; then  \
	   echo "There is no change on python file."; \
	else \
	   python3 -m pylint --rcfile=.pylintrc $(DIFF_FILES); \
	fi

build_app: app.template.yaml
ifdef SHOW_APP_YAML
	@envsubst < app.template.yaml > app.yaml && echo "===== Start app.yaml=====" && cat app.yaml && echo "===== End of app.yaml ====="
else
	@envsubst < app.template.yaml > app.yaml
endif

