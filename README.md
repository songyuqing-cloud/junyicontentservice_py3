<div align="center">
<h1>Junyi Academy Content Service</h1>

<a href="https://junyiacademy.org">
  <img
    width="300px"
    alt="junyi academy logo"
    src="https://www.junyiacademy.org/images/logo_256.png"
  />
</a>

<p>Junyi Academy is a nonprofit organization.</p>
<p>This project is our open source project for backend content service.</p>

<br />
</div>

<hr />

<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![MIT License][license-badge]][license]
[![All Contributors](https://img.shields.io/badge/all_contributors-16-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

## Contributors ✨

Thanks goes to these wonderful people ([emoji key][emoji-key]):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/aimichen"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1702488/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Amy Chen</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a> <a href="#infra-aimichen" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=aimichen" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/GeneTso"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1708304/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Gene</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a> <a href="#infra-GeneTso" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=GeneTso" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/kerker1"><img src="https://secure.gravatar.com/avatar/d82767e05bae68908cd8ac1abd7f1100?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>kerker</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=kerker1" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/kamesan1"><img src="https://secure.gravatar.com/avatar/c92844066b4a5fdfe2e5b3c80189ee83?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>Ren-Pei Zeng</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=kamesan1" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/linamy85"><img src="https://secure.gravatar.com/avatar/a3993def6bb4c9e8eae05cb20365d816?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>Amy Lin</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=linamy85" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/ericth"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5753252/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Tzu Hsiang Lin</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=ericth" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/lambdaTW"><img src="https://secure.gravatar.com/avatar/707826cbe0dabad3cbdc3b285659a514?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>lambdaTW</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="#infra-lambdaTW" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=lambdaTW" title="Reviewed Pull Requests">👀</a></td>
  </tr>
  <tr>
    <td align="center"><a href="https://gitlab.com/jubeatWwW"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1708471/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Justin</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a></td>
    <td align="center"><a href="https://gitlab.com/yisheng.cpbr"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1708340/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>yishengJiang</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a></td>
    <td align="center"><a href="https://gitlab.com/98Chamberlain"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1708306/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Chen Chamberlain</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a></td>
    <td align="center"><a href="https://gitlab.com/chengying.chang"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3285129/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>張正穎</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a> <a href="#infra-chengying.chang" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=chengying.chang" title="Reviewed Pull Requests">👀</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a></td>
    <td align="center"><a href="https://gitlab.com/derek.zhan"><img src="https://secure.gravatar.com/avatar/941e869c1a7c4fadabed9d9d8df4bb0a?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>Derek Zhan (Kai)</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a></td>
    <td align="center"><a href="https://gitlab.com/leo.lin1"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6003090/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>林暐唐</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a></td>
    <td align="center"><a href="https://gitlab.com/jphua"><img src="https://secure.gravatar.com/avatar/8574f15433ce6b92890214cf50162ee4?s=100&d=identicon" width="100px;" alt=""/><br /><sub><b>Josh Phua</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests?scope=all&state=all&approver_usernames[]=jphua" title="Reviewed Pull Requests">👀</a></td>
  </tr>
  <tr>
    <td align="center"><a href="https://gitlab.com/isacl.li"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/7550152/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>李宥辰</b></sub></a><br /><a href="#infra-isacl.li" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a></td>
    <td align="center"><a href="https://gitlab.com/Cowbon_junyiacademy"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/2531484/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>Lin Chih-Heng</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Documentation">📖</a> <a href="https://gitlab.com/junyiacademy/junyicontentservice_py3/commits/master" title="Code">💻</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors][all-contributors] specification. Contributions of any kind welcome!

## LICENSE

[MIT][license]


[license-badge]: https://img.shields.io/badge/license-MIT-green?style=flat-square
[license]: https://gitlab.com/junyiacademy/junyicontentservice_py3/-/blob/master/LICENSE
[emoji-key]: https://allcontributors.org/docs/en/emoji-key
[all-contributors]: https://github.com/all-contributors/all-contributors
