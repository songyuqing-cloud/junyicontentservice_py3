### Release Note

#### Upcoming release candidates

###### Opened merge requests:
- [MR #101](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/101) (lambdaTW): Refactor: Use environment variables to store sensitive data
- [MR #97](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/97) (lambdaTW): Fixed: Add tests for video content_rights_object
- [MR #92](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/92) (Gene): New: Add service cache - 2 - implement redis cache class
- [MR #73](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/73) (Amy Lin): Changed: Fix datastore read limitation = 1000
- [MR #66](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/66) (Tzu Hsiang Lin): New: Migrate play videos API from "JunyiContentService" to "JunyiContentService_py3"

###### Approved merge requests:
- [MR #106](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/106) (lambdaTW): Refactor: Feature refactor check local server
- [MR #83](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/83) (Gene): New: Add service cache - 1 - porting layer cache
- [MR #61](https://gitlab.com/junyiacademy/junyicontentservice_py3/-/merge_requests/61) (Amy Chen): Fixed: topictreedata api response when topics have sections

###### Merged merge requests:
- [MR #109](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/109) (Gene): Changed: Return 404 if topic is hidden
- [MR #108](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/108) (Gene): Changed: Rename production version name
#### 1.0.11-2020-09-17-1530-production1
###### Released at 2020-09-15. Included merge requests:
- [MR #107](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/107) (Gene): merge staging
- [MR #103](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/103) (Gene): Fixed: fix video creation failure when parent topic doesn't have 'child_keys'
- [MR #104](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/104) (Gene): Refactor: Refine get topic grandchild implementation - 2

#### 1.0.10-2020-09-04-1020-production3
###### Released at 2020-09-03. Included merge requests:
- [MR #102](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/102) (Gene): merge staging
- [MR #98](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/98) (Gene): Changed: Refine get topic grandchild implementation

#### 1.0.9-2020-08-31-1625-production2
###### Released at 2020-08-31. Included merge requests:
- [MR #100](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/100) (Gene): merge staging
- [MR #99](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/99) (Gene): Changed: Remove get topic grandchild implementation
- [MR #96](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/96) (Gene): merge staging into master
- [MR #95](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/95) (Gene): update README
- [MR #91](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/91) (Gene): fix datastore emulator in local server
- [MR #93](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/93) (Gene): Update local db for ci test

#### 1.0.8-2020-07-21-1030-production3
###### Released at 2020-07-20. Included merge requests:
- [MR #94](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/94) (Gene): merge staging
- [MR #90](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/90) (Gene): add api tests for topic page api
- [MR #87](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/87) (Gene): add grandchild to Topic.info_to_topic_page

#### 1.0.7-2020-07-16-1030-production2
###### Released at 2020-07-15. Included merge requests:
- [MR #89](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/89) (Gene): merge staging into master
- [MR #88](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/88) (Gene): add topic desc for compatibility
- [MR #86](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/86) (Gene): refactor topic test
- [MR #82](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/82) (Gene): Fix get topic api /api/content/topicpage
- [MR #68](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/68) (Gene): studygroup 0618 02
- [MR #85](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/85) (Gene): add intro to child topic in /api/content/topicpage
- [MR #84](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/84) (Ren-Pei Zeng): Fix issue #79
- [MR #76](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/76) (Ren-Pei Zeng): Fix issue #80

#### 1.0.6-2020-07-07-1030-production1
###### Released at 2020-07-06. Included merge requests:
- [MR #81](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/81) (Gene): merge staging into master
- [MR #80](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/80) (Gene): exclude content_changes from index
- [MR #75](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/75) (Gene): Retry get topic entity in main test
- [MR #79](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/79) (Gene): Fix video prop extra_properties
- [MR #74](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/74) (Gene): add updated_on field in version content change
- [MR #77](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/77) (Gene): replace datetime.now() by datetime.utcnow()
- [MR #71](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/71) (kerker): retry video test when checking with db content
- [MR #56](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/56) (Gene): Use workaround for ancestor query in local server
- [MR #70](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/70) (Gene): use pickle protocol 2 to be compatible to python 2
- [MR #69](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/69) (kerker): Add video db columns
- [MR #50](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/50) (kerker): Create update video api
- [MR #54](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/54) (Chen Chamberlain): 同步 junyiacademy!2758 的 topic tags 限制
- [MR #65](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/65) (Gene): use pickle protocol 2 to be compatible to python 2
- [MR #63](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/63) (yishengJiang): Update README.md for run redis guide
- [MR #46](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/46) (Amy Lin): Topic api
- [MR #45](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/45) (Ren-Pei Zeng): Batch exercise api
- [MR #62](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/62) (Ren-Pei Zeng): fix makefile phony targets
- [MR #60](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/60) (Gene): delete staging version before deploying new one
- [MR #59](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/59) (Gene): add _get_topic_entity_by_id_retry
- [MR #52](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/52) (Gene): Add backend test to GitLab CI
- [MR #58](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/58) (Gene): update topic init links test
- [MR #57](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/57) (Amy Chen): topicpage api support section on edit/default version
- [MR #53](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/53) (Gene): Fix init topic links
- [MR #55](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/55) (Gene): add app.yaml for test server
- [MR #51](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/51) (Amy Chen): fix assertion
- [MR #47](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/47) (Gene): setup redis server
- [MR #49](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/49) (Amy Chen): Section limitation
- [MR #48](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/48) (Amy Chen): Add section support
- [MR #43](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/43) (Amy Chen): Add section api
- [MR #44](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/44) (Amy Chen): Refac for section

#### 1.0.5-2020-04-27-1520-production3
###### Released at 2020-04-24. Included merge requests:
- [MR #42](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/42) (Gene): Staging
- [MR #37](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/37) (Gene): skip cache if get edit version topic
- [MR #41](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/41) (Amy Chen): Strict create topic
- [MR #36](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/36) (Gene): add skip_cache in instance_cache
- [MR #38](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/38) (Gene): only allow moderator get edit version
- [MR #39](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/39) (Amy Chen): refine readme
- [MR #40](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/40) (Amy Chen): 根據 restful api 建議，區分 401 Unauthorized 和 403 Forbidden
- [MR #35](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/35) (Gene): get edit version topic page data
- [MR #34](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/34) (Amy Chen): Add doc
- [MR #33](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/33) (Gene): Fix exercise without pretty_display_name
- [MR #32](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/32) (Gene): Reformat code

#### 1.0.4-2020-04-13-1230-production2
###### Released at 2020-04-13. Included merge requests:
- [MR #31](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/31) (Gene): Staging
- [MR #30](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/30) (Gene): add create topic test case
- [MR #29](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/29) (Gene): Fix batch create
- [MR #25](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/25) (Gene): add internal topic tests
- [MR #24](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/24) (Gene): add batch api test
- [MR #27](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/27) (Gene): add get topic logo data
- [MR #28](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/28) (Gene): Get ordered ancestors
- [MR #23](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/23) (Gene): Reformat code
- [MR #21](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/21) (Gene): merge staging
- [MR #22](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/22) (Amy Chen): fix and add test case
- [MR #17](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/17) (Amy Chen): 搬 api '/api/content/topictreedata'
- [MR #20](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/20) (Gene): parse json string in topic.links

#### 1.0.3-2020-03-27-1530-production1
###### Released at 2020-03-25. Included merge requests:
- [MR #19](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/19) (Amy Chen): Staging
- [MR #18](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/18) (Gene): add new topic props
- [MR #16](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/16) (Amy Chen): Breadcrumb add tags
- [MR #15](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/15) (Amy Chen): access control
- [MR #14](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/14) (Amy Chen): Fkey
- [MR #11](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/11) (Amy Chen): fix no ancestor error
- [MR #12](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/12) (Gene): add batch update topic api
- [MR #9](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/9) (Gene): add batch create topic api
- [MR #13](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/13) (Gene): merge staging
- [MR #10](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/10) (Gene): /api/content/topicpage 缺 GET 參數時回覆 400
- [MR #4](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/4) (Gene): get default version by setting
- [MR #6](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/6) (Gene): remove cool english fix
- [MR #5](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/5) (Gene): Unify hidden tag
- [MR #7](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/7) (Gene): add debug log for exercise without pretty name
- [MR #8](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/8) (Amy Chen): Fixunittest
- [MR #3](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/3) (Gene): update README.md
- [MR #2](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/2) (Amy Chen): Gitlab ci 和其他
- [MR #1](https://gitlab.com/junyiacademy/junyicontentservice_py3/merge_requests/1) (Amy Chen): Auth by jaid
