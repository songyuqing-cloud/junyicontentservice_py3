## [Reference](https://flask.palletsprojects.com/en/1.1.x/config/#)
### How to get the config in the right way
```python3
from flask import current_app

current_app.config.get('KEY_OF_CONFIG_NAME', None)
```
### Example: Check current APP is running in local server or not
```python3
from flask import current_app


if current_app.config.get('IN_LOCAL', False):
    # Do something
```
