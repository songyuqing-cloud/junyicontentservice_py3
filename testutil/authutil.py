# -*- coding: utf-8 -*-
import time

from google.auth import crypt, jwt


def create_jaid(moderator=False, developer=False, expired=False, future=False):
    if expired and future:
        raise ValueError('expired 和 future 最多一項為 True')

    now = int(time.time())
    if expired:
        now = now - 3600 * 24 * 14 - 1
    if future:
        now = now + 3600
    payload = {'sub': 'FAKE_USER_DATA_KEY', 'iat': now, 'exp': now + 3600}
    if moderator:
        payload['moderator'] = True
    if developer:
        payload['developer'] = True

    with open('api_client/default_dev_private.pem') as fd:
        key = fd.read()
    signer = crypt.RSASigner.from_string(key)

    return jwt.encode(signer, payload)