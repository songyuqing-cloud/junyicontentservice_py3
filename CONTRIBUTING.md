# Getting Started

## Setup local server evnironment

請參考新手上路文件，設定好 docker 環境並啟動 datastore emulator

[設定 Local Server 環境](https://www.notion.so/junyiacademy/2-Local-Server-941dd5321a5e424985e98061d9dd9bed)

[啟動 Docker](https://www.notion.so/junyiacademy/3-Docker-Local-Server-921d00be23734f5f8ef42ade222e884f)

## Export required environment variables
若有需要，請跟專案管理人員要 `.env.example.sh` 內的值，並改寫到 `.env.sh` 然後再跑專案或是測試前執行它。注意：請勿將 `.env.sh` 加入 git 中

### Example

```shell-script
docker $ mv .env.example.sh .env.sh
docker $ # Edit value
docker $ vi .env.sh
docker $ source .env.sh
docker $ python3 tools/runtests.py
```

## Run services

### 單獨啟動 content service（推薦 content service 開發者使用）

另開一個 bash 啟動 flask，注意這樣會啟動在 8010 port

    $ docker exec -it content_py3 bash

[設定環境變數](https://www.notion.so/junyiacademy/3-Docker-Local-Server-921d00be23734f5f8ef42ade222e884f#f17bc159c086421ea9e5cebacec90e16)

安裝 dependency // FIXME: should be included in docker image

    docker $ pip3 install -r requirements.txt

啟動 server

    docker $ cd /src
    docker $ export FLASK_ENV=development; python3 main.py

測試

    $ curl localhost:8010/api/content/hello

如果需要手動測試其他 API ，則需加入 XSRF header

    $ curl localhost:8010/api/content/topic -H 'x-ka-fkey: 123' -H 'Cookie: fkey=123'

### 同時啟動多個服務，可以完整架起本地的均一教育平台

請參考 [啟動使用 contentservice_py3 的 local server](https://www.notion.so/junyiacademy/3-Docker-Local-Server-921d00be23734f5f8ef42ade222e884f#9223191c623c4d47af24c49ec2097f2a)

warmup 失敗 （`ERROR:main:Exception on /_ah/warmup [GET]`）很常見，只要 `contentfrontendpy3` 比 `default` 早就緒就會發生，可以忽略。

## Run tests

### Run all tests
    docker $ python3 tools/runtests.py

### Run single test file
    docker $ python3 tools/runtests.py content/internal/topic_test.py
    docker $ python3 tools/runtests.py content.internal.topic_test

### Run single test case
    docker $ python3 tools/runtests.py content.internal.topic_test.TestTopic
    docker $ python3 tools/runtests.py content.internal.topic_test.TestTopic.test_init_by_invalid_topic_id

### Run coverage
install package // FIXME: should be included in docker image

    docker $ pip3 install coverage

run

    docker $ coverage3 run tools/runtests.py content.internal.topic_test.TestTopic

或

    docker $ coverage3 run tools/runtests.py

report

    docker $ coverage3 report

或
 
    docker $ coverage3 html

## Troubleshoot
### 無法啟動 Flask / auto test fail

#### google.auth.exceptions.DefaultCredentialsError

```
google.auth.exceptions.DefaultCredentialsError: Could not automatically determine credentials. Please set GOOGLE_APPLICATION_CREDENTIALS or explicitly create credentials and re-run the application. For more information, please see https://cloud.google.com/docs/authentication/getting-started
```
docker 內沒有 `/.config/gcloud/application_default_credentials.json`，
呼叫到 google sdk 時就會發生以下錯誤；做一次 `gcloud auth application-default login` 即可


#### OSError
```
OSError: Project was not passed and could not be determined from the environment.
```
執行 Flask / 跑 test 的 bash 沒有設好環境變數，沒有連上 datastore emulator；請參考
[設定環境變數](https://www.notion.so/junyiacademy/3-Docker-Local-Server-921d00be23734f5f8ef42ade222e884f#f17bc159c086421ea9e5cebacec90e16)

## Documents

Please also read the following guides before contributing

https://gitlab.com/junyiacademy/junyi-docs/-/tree/master/content-service
